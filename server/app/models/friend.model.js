import mongoose from 'mongoose'

const Schema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now()
    },
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    message: String
})

export default mongoose.model('Friend', Schema);