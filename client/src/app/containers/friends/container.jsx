import React, {Component} from "react"
import {connect} from "react-redux"
import PropTypes from "prop-types"
import { withRouter } from "react-router-dom"

// import compoments
import Button from "../../components/button"

// partial
import FriendsList from "./partials/friends-list"

class Friends extends Component {
	constructor(props){
		super(props)

		this.handleAddClick = this.handleAddClick.bind(this)
	}

	handleAddClick() {
		this.props.history.push('/friend/add')
	}

	render() {

		return (
			<div>
				<section>
					<div className="container">
						<h1 className="jumbotron-heading">My Friends</h1>
						<p className="lead text-muted">List of all my professional friends - Please handle them with care!</p>
						<p>
							<Button onClick={this.handleAddClick} className="btn-primary">Add new friend</Button>
						</p>
					</div>
					<FriendsList />
				</section>
			</div>
		)
	}
}


Friends.propTypes = {
	history: PropTypes.object
}

export default withRouter(connect()(Friends))