import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'

configure({ adapter: new Adapter() })

import AddFriendForm from '../add-friend-form'

/* eslint-env jest */

test('AddFriendForm renders without errors', () => {
    const wrapper = shallow(<AddFriendForm/>)
    expect(wrapper.length).toBe(1)
})

