import React, { Component } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { createPropsSelector } from "reselect-immutable-helpers"
import { withRouter } from "react-router-dom"

// utils
import {getEndPointURL} from "../../../utils/utils"

// custom components
import Card from "../../../components/card"
import Image from "../../../components/image"
import Button from "../../../components/button"

// actions 
import {
	fetchFriendsData,
	onDeleteFriend,
	setFriendsId
} from "../actions"

// selectors
import {getFriendsList, getShouldMakeRequest} from "../selectors"

class FriendsList extends Component {

	constructor(props) {
		super(props)

		// @todo pass endpoint url to store state
		this.api = `${getEndPointURL()}/api/friends`
		this.deleteFriend = this.deleteFriend.bind(this)
		this.updateFriend = this.updateFriend.bind(this)
	}

	componentDidMount() {
		// ONLY make api request when needed
		this.props.shouldMakeRequest &&
			this.props.fetchFriendsData(this.api)
	}

	updateFriend(id) {
		// @todo move logic to edit friend page and read from database
		// if page is visited directly
		Promise.resolve(this.props.setFriendsId(id))
		.then(() => this.props.history.push(`/friend/edit/${id}`))
	}

	deleteFriend(id) {
		this.props.onDeleteFriend(this.api, {id})
	}

	loadFriendData(id) {
		// push history
		this.props.history.push(`/friend/${id}`)
	}

	render() {
		const {
			friends
		} = this.props

		return (
			<div className="container">
				<div className="row">
					{
						(Object.keys(friends).length > 0) &&
							Object.keys(friends).map((idx) => (
								<div className="col-md-4 mb-5" key={idx}>
										<Card
											image={<Image src='http://via.placeholder.com/210x200?text=Thumbnail' />}
										>
											<h4>{friends[idx].firstName} {friends[idx].lastName}</h4>
											<p>{friends[idx].message}</p>
											<div className="d-flex justify-content-between align-items-center">
												<div className="btn-group">
													<Button className="btn btn-sm btn-primary" onClick={() => this.loadFriendData(friends[idx]._id)}>View</Button>
													<Button className="btn btn-sm btn-info" onClick={() => this.updateFriend(friends[idx]._id)}>Edit</Button>
													<Button className="btn-sm btn-danger" onClick={() => this.deleteFriend(friends[idx]._id)}>Delete</Button>
												</div>
												<small className="text-muted">9 mins</small>
											</div>
										</Card>
									</div>
                            ))
					}
				</div>
			</div>
		)
	}
}

FriendsList.propTypes = {
	fetchFriendsData: PropTypes.func,
	friends: PropTypes.object,
	history: PropTypes.object,
	onDeleteFriend: PropTypes.func,
	setFriendsId: PropTypes.func,
	shouldMakeRequest: PropTypes.bool,
}

const mapStateToProps = createPropsSelector({
	friends: getFriendsList,
	shouldMakeRequest: getShouldMakeRequest
})

const mapDispatchToProps = {
	fetchFriendsData,
	onDeleteFriend,
	setFriendsId
}

export default withRouter(connect(
	mapStateToProps,
	mapDispatchToProps
)(FriendsList))


