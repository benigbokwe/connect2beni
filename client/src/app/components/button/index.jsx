import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

const Button = ({
    className, 
    onClick,
    children,
    type,
    disabled
}) => {
    const classes = classNames('btn', {}, className)

    return (
        <button
            type={type}
            className={classes} 
            onClick={onClick}
            disabled={disabled}
        >
            {children}
        </button>
    )
}

Button.defaultProps = {
    type: "button"
}

Button.propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.string
}

export default Button