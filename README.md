# CRUD OPERATION USING MERRN STACK
    - MongoDB, ExpressJs, React, Redux, NodeJs

Restful API's built using Express.js and MongoDB on the server side

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
node v6.x, mongodb server
```

### Installing

A step by step series of examples that tell you have to get a development env running

Clone repository

```
git clone https://benigbokwe@bitbucket.org/benigbokwe/connect2beni.git
```

This project has two main folders - client and server folders for rendering views and handling requests respectively

```
cd pathtoclone/client
```
```
pathtoclone/client > npm install
pathtoclone/client > npm start
```

Then open new terminal and navigate to 
```
cd pathtoclone/server
```
```
pathtoclone/server > npm install
pathtoclone/server > npm start
```

Most of the links will return 404 error page because they havnt been implemented yet
Click on burger menu and then on My friends link - 

## Test
Not much have been done but a few components have been tested to ensure they are rendered without errors - ONLY in client folder
```
pathtoclone/client > npm test
```


## Note
- MongoDB server must be started for the server to work - 
- Reducers and actions creators to be worked on 


## Authors

* **Ben Igbokwe**