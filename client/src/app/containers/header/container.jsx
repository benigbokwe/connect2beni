import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => (
  <header>
    <div className="bg-dark collapse" id="navbarHeader">
      <div className="container">
        <div className="row">
          <div className="col-sm-8 py-4">
            <h4 className="text-white">About me</h4>
            <p className="text-muted">
                Bla bla bal about myself!
            </p>
          </div>
          <div className="col-sm-4 py-4">
            <h4 className="text-white">{' <Links />'}</h4>
            <ul className="list-unstyled">
              <li>
                <Link to="/friends" className="text-white">
                    My friends
                </Link>
              </li>
              <li>
                <Link to="/jobs" className="text-white">
                  Jobs
                </Link>
              </li>
              <li>
                <Link to="/contact" className="text-white">
                  Contact me
                </Link>
              </li>
              <li>
                <Link to="signin" className="text-blue btn btn-primary mr-3">
                  Sign in
                </Link>
                <Link to="/post-a-job" className="text-blue btn btn-danger">
                  Post a job
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div className="navbar navbar-dark bg-dark">
      <div className="container d-flex justify-content-between">
        <Link to="/" className="navbar-brand">
          {'{Connect2Beni}'}
        </Link>
        <button
            className="navbar-toggler collapsed"
            type="button"
            data-toggle="collapse"
            data-target="#navbarHeader"
            aria-controls="navbarHeader"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
      </div>
    </div>
  </header>
)

export default Header
