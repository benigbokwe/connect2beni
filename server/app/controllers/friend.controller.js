import mongoose from 'mongoose';
//import models
import Friend from '../models/friend.model'

export const getFriends = (req, res) => {
    Friend.find().exec((err, friends) => {
    if (err) {
        return res.json({
            'success':false,
            'message':'Some Error has occured'
        })
    }

    return res.json({
        'success':true,
        'message': 'Friends fetched successfully',
        friendsList: friends
    })
  })
}

export const addFriend = (req, res) => {
    const newFriend = new Friend(req.body)

    newFriend.save((err, friend) => {
        if (err) {
        return res.json({
            'success':false,
            'message':'Some Error'
        })
    }

    return res.json({
      'success': true,
      'message': 'Friend added successfully',
      friend
    })
  })
}

export const updateFriend = (req, res) => {
    Friend.findOneAndUpdate({ _id:req.body._id }, req.body, { new:true }, (err, friend) => {
        if (err) {
            return res.json({
              'success':false,
              'message':'Some Error',
              'error':err
            })
        }

        return res.json({
            'success':true,
            'message': 'Updated successfully',
            friend
        })
    })
}

export const getFriend = (req, res) => {
    Friend.find({_id: req.params.id}).exec((err,friend) => {
      if (err) {
        return res.json({
          'success': false,
          'message': 'Some Error'
        })
      }

      if (friend.length) {
        return res.json({
          'success': true,
          'message': 'Friend fetched by id successfully',
          friendsList: friend
        })
      } else {
        return res.json({
          'success': false,
          'message':'Friend with the given id not found'
        })
      }
    })
  }

  export const deleteFriend = (req, res) => {
    Friend.findByIdAndRemove(req.params.id, (err, friend) => {
      if(err) {
        return res.json({
          'success': false,
          'message':'Some Error'
        })
      }
  
      return res.json({
        'success': true,
        'message': 'Deleted successfully'
      })
    })
  }