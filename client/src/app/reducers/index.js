import { combineReducers } from "redux"

// import reducers
import friendsReducer from "../containers/friends/reducer"

const rootReducer = combineReducers({
	friends: friendsReducer
})

export default rootReducer