import { createAction } from "redux-actions"

export const receiveFriendsData = createAction("Receive friends data")
export const saveFriendData = createAction("Save friend\\'s data")
export const deleteFriendData = createAction("Delete friend\\'s data from store")
export const updateFriendData = createAction("Update friend\\'s data from store")