import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {createPropsSelector} from 'reselect-immutable-helpers'
import { Link } from 'react-router-dom'

// partials
import EditFriendForm from './partials/edit-friend-form'

// actions
import { editFriendsData, fetchFriendsData, setFriendsId } from '../friends/actions'

// utils
import {getEndPointURL} from '../../utils/utils'

// selectors
import {getFormInitialValues, getShouldMakeRequest} from '../friends/selectors'

class EditFriend extends Component {

    constructor(props) {
        super(props)
        this.onSubmit = this.onSubmit.bind(this)

        this.state = {
            formSumitted: false
        }

        this.api = `${getEndPointURL()}/api/friends`
    }

    componentDidMount() {
        // ONLY make api request when needed
        const id = this.props.match.params.id
		if(this.props.shouldMakeRequest) {
            Promise.resolve(this.props.setFriendsId(id)).then(() => {
                this.props.fetchFriendsData(`${this.api}/${id}`)
            })
        }
            
    }

    onSubmit(values) {
        // @todo - pass enpoint url to state
        // validate form before resolving
        Promise.resolve(this.props.editFriendsData(this.api, values))
        .then(() => {
            this.setState({
                formSumitted: true
            })
        })
    }

    render() {
        const {
            initialValues
        } = this.props
    
        return (
            <div className="container">
                {
                    this.state.formSumitted &&
                    <div>
                        <Link className="btn btn-info mt-5 mb-5" to="/friends"> View friends list </Link>
                        <div className="alert alert-success" role="alert">
                            Friend has updated added successfully!
                        </div>
                    </div> 
                }
                <section className="mt-5 mb-5">
                    <h1 className="display-4">Edit Friend</h1>
                    <EditFriendForm onSubmit={this.onSubmit} initialValues={initialValues} />
                </section>
            </div>
        )
    }
}

EditFriend.propTypes = {
    initialValues: PropTypes.object,
    editFriendsData: PropTypes.func,
    fetchFriendsData: PropTypes.func,
    match: PropTypes.object,
    setFriendsId: PropTypes.func,
    shouldMakeRequest: PropTypes.bool,
}

const mapStateToProps = createPropsSelector({
    initialValues: getFormInitialValues,
    shouldMakeRequest: getShouldMakeRequest
})

const mapDispatchToProps = {
    editFriendsData,
    fetchFriendsData,
    setFriendsId
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditFriend)

