import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import logger from 'morgan'
import mongoose from 'mongoose'
import bb from 'express-busboy'
import SourceMapSupport from 'source-map-support'

// import routes
import routes from './router'

const app = express()

// express-busboy to parse multipart/form-data
bb.extend(app)

// allow-cors
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
    next()
})

// configure app
app.use(logger('dev'))
// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))


// set the port
const port = process.env.PORT || 3002

// @todo add in mongoose config file
const uri = 'mongodb://127.0.0.1/connect2beni'
const options = {
    socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30
  }

// connect to database
mongoose.Promise = global.Promise
mongoose.connect(uri, options, (err) => {
    if(err) {
        console.log('Error has occured: ', err)
    }
})

// add Source Map Support
SourceMapSupport.install()

app.use('/api', routes)
app.get('/', (req, res) => {
    return res.end('Api for connect2beni is working')
})

// catch 404
app.use((req, res, next) => {
    res.status(404).send('<h2 align=center>Page Not Found!</h2>')
})

// start the server
app.listen(port, () => {
    console.log(`App Server Listening at ${port}`)
});