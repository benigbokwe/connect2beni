import { createStore, applyMiddleware, compose, combineReducers } from "redux"
import thunk from "redux-thunk"
import { createLogger } from "redux-logger"
import rootReducer from "../app/reducers"

import { reducer as formReducer } from "redux-form"
// import DevTools from "../app/tools/DevTools"

const configureStore = preloadedState => {
	const reducer = combineReducers({
		ui: rootReducer,
		form: formReducer
	})

	const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

	const store = createStore(
		reducer,
		preloadedState,
		composeEnhancers(applyMiddleware(thunk, createLogger()))
	)

	if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		// module.hot.accept('../app/reducers', () => {
		//  store.replaceReducer(reducer)
		//})
	}

	return store
}

export default configureStore
