import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import App from './containers/app/container'

import Home from './containers/home/container'
import About from './containers/about/container'
import Friend from './containers/friend/container'
import AddFriend from './containers/friend/add-friend'
import EditFriend from './containers/friend/edit-friend'
import Friends from './containers/friends/container'
import Jobs from './containers/jobs/container'
import NotFound from './containers/notfound/container'

const Routes = ({ store }) => (
  <Provider store={store}>
    <Router>
      <App>
        <Switch>
          <Route exact={true} path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/friend/add" component={AddFriend} />
          <Route path="/friend/edit/:id" component={EditFriend} />
          <Route path="/friend/:id" component={Friend} />
          <Route path="/friends" component={Friends} />
          <Route path="/jobs" component={Jobs} />
          <Route path="*" component={NotFound} />
        </Switch>
      </App>
    </Router>
  </Provider>
);

Routes.propTypes = {
    store: PropTypes.object
};

export default Routes