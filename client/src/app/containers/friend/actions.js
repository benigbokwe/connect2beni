
// action creators
import {
	receiveFriendsData
} from "../../actions/create-actions"

export const setFriendId = (id) => (dispatch) => {
	return dispatch(receiveFriendsData({_id: id}))
}