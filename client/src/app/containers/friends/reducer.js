import { handleActions } from "redux-actions"
import Immutable, {Map} from "immutable"
import { mergePayload } from "../../utils/reducer-utils"
import { 
	receiveFriendsData,
	deleteFriendData,
	saveFriendData,
	updateFriendData 
} from "../../actions/create-actions"

export default handleActions({
		[receiveFriendsData]: mergePayload,
		[saveFriendData]: mergePayload,
		[deleteFriendData]:  (state, {payload}) => {
			const newSate = Map({
				friendsList: state.get('friendsList').filter((item, id) => id !== payload._id )
			})

			return newSate.mergeDeep(payload)
		},
		[updateFriendData]: (state, {payload}) => {
			const newSate = Map({
				...state.map((item, idx) => {
					return payload
				})
			})

			return newSate.mergeDeep(payload)
		}
	},
	Immutable.Map()
)