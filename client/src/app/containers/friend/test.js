import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import {Provider} from 'react-redux'

configure({ adapter: new Adapter() })

import Friend from './container'
import EditFriend from './edit-friend'
import AddFriend from './add-friend'

/* eslint-env jest */

const store = {
    subscribe: () => {},
    dispatch: () => {},
    getState: () => ({ })
}

test('Friend renders without errors', () => {
    const wrapper = shallow(<Provider store={store}><Friend /></Provider>)
    expect(wrapper.length).toBe(1)
})


test('EditFriend renders without errors', () => {
    const wrapper = shallow(<Provider store={store}><EditFriend /></Provider>)
    expect(wrapper.length).toBe(1)
})


it('AddFriend renders without errors', () => {
    const wrapper = shallow(<Provider store={store}><AddFriend /></Provider>)
    expect(wrapper.length).toBe(1)
})

