import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'

configure({ adapter: new Adapter() })

import FriendsList from '../../partials/friends-list'

/* eslint-env jest */

const props = {
    history: {
        push: '/test/'
    }
}

test('FriendsList renders without errors', () => {
    const wrapper = shallow(<FriendsList {...props}/>)
    expect(wrapper.length).toBe(1)
})

