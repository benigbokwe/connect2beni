
export const ADD = 'ADD'
export const UPDATE = 'UPDATE'
export const DELETE = 'DELETE'
export const DISPLAY = 'DISPLAY'

export const ACTION_TYPES = [
    ADD,
    UPDATE,
    DELETE
]