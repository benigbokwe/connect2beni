import express from 'express'
//import controller file
import * as controller from './controllers/friend.controller'

// get an instance of express router
const router = express.Router()

router.route('/friends')
     .get(controller.getFriends)
     .post(controller.addFriend)
     .put(controller.updateFriend)

router.route('/friends/:id')
      .get(controller.getFriend)
      .delete(controller.deleteFriend)

export default router