import React, {Component} from "react"
import {connect} from "react-redux"
import PropTypes from "prop-types"
import { createPropsSelector } from "reselect-immutable-helpers"

// import components
import Button from '../../components/button'

// selectors
import {getFriendData, getShouldMakeRequest} from '../friends/selectors'

// actions
import {setFriendId} from './actions'
import {fetchFriendsData} from '../friends/actions'

// utils
import {getEndPointURL} from '../../utils/utils'

class Friend extends Component {
    constructor(props) {
        super(props)

        this.api = `${getEndPointURL()}/api/friends`
    }

    componentDidMount(){
         // ONLY make api request when needed
         const id = this.props.match.params.id
        this.props.setFriendId(id)

        if (this.props.shouldMakeRequest) {
            this.props.fetchFriendsData(`${this.api}/${id}`)
        }
    }

    render() {
        const {
            friend
        } = this.props

        console.log(friend)

        return (
            <div>
                {friend && Object.keys(friend).length > 0 &&
                    <section>
                        <div className="container">
                            <div className="jumbotron">
                                <h1 className="display-3">{friend.firstName} {friend.lastName}</h1>
                                <p className="lead"> {friend.message}</p>
                                <p className="lead"> {friend.email}</p>
                                <p>
                                    <Button className="btn-lg btn-success">Ping him!</Button>
                                </p>
                            </div>
                        </div>
                    </section>
                }
            </div>
        )
        }
    }

Friend.propTypes = {
    fetchFriendsData: PropTypes.func,
    friend: PropTypes.object,
    match: PropTypes.object,
    setFriendId: PropTypes.func,
    shouldMakeRequest: PropTypes.bool,
}

const mapStateToProps = createPropsSelector({
    friend: getFriendData,
    shouldMakeRequest: getShouldMakeRequest
})

const mapDispatchToProps = {
    setFriendId,
    fetchFriendsData
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Friend)