import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

// partials
import AddFriendForm from './partials/add-friend-form'

// actions
import { addFriendsData } from '../friends/actions'

// utils
import {getEndPointURL} from '../../utils/utils'

class AddFriend extends Component {

    constructor(props) {
        super(props)
        this.onSubmit = this.onSubmit.bind(this)

        this.state = {
            formSumitted: false
        }
    }

    onSubmit(values) {
        // @todo - pass enpoint url to state
        // validate form before resolving
        const api = `${getEndPointURL()}/api/friends`
        Promise.resolve(this.props.addFriendsData(api, values))
        .then(() => {
            this.setState({
                formSumitted: true
            })
        })
    }

    render() {

        return (
            <div className="container">
                {
                    this.state.formSumitted &&
                    <div>
                        <Link className="btn btn-info mt-5 mb-5" to="/friends"> View friends list </Link>
                        <div className="alert alert-success" role="alert">
                            Friend has been added successfully!
                        </div>
                    </div> 
                }
                <section className="mt-5 mb-5">
                    <h1 className="display-4">Add New Friend</h1>
                    <AddFriendForm onSubmit={this.onSubmit}/>
                </section>
            </div>
        )
    }
}

AddFriend.propTypes = {
    addFriendsData: PropTypes.func
}

const mapDispatchToProps = {
    addFriendsData
}

export default connect(
    null,
    mapDispatchToProps
)(AddFriend)

