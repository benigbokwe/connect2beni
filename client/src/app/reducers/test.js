import reducers from './index'

/* eslint-env jest */

test('reducer combines reducers', () => {

    const action = {
        type: 'RECEIVE_DATA',
        payload: {
            friendsList: {}
        }
    }

    const state = {
        friends: {
            friendsList: {} 
        }
    }

    expect(reducers(state, action)).toEqual(state)
})