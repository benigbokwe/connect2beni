import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'

configure({ adapter: new Adapter() })

import Friends from './container'

/* eslint-env jest */

const props = {
    history: {
        push: '/test/'
    }
}

test('Friends renders without errors', () => {
    const wrapper = shallow(<Friends {...props}/>)
    expect(wrapper.length).toBe(1)
})

