
// connectors
import { 
	initFriendsData,
	initSaveFriendsData,
	initDeleteFriendsData,
	initUpdateFriendsData
} from "../../connectors/custom-commands"

// action creators
import {
	receiveFriendsData,
	updateFriendData
} from "../../actions/create-actions"

export const fetchFriendsData = (url) => (dispatch) => {
	return dispatch(initFriendsData(url))
}

export const fetchAFriendData = (url) => (dispatch) => {
	return dispatch(initFriendsData(url))
}

export const addFriendsData = (url, data) => (dispatch) => {
	dispatch(receiveFriendsData({shouldMakeRequest: false}))
	return dispatch(initSaveFriendsData(url, data))
}

export const editFriendsData = (url, data) => (dispatch) => {
	dispatch(receiveFriendsData({shouldMakeRequest: false}))
	return dispatch(initUpdateFriendsData(url, data))
}

export const onDeleteFriend = (url, data) => (dispatch) => {
	const {id} = data
	const api = `${url}/${id}`
	return dispatch(initDeleteFriendsData(api, id))
}

export const setFriendsId = (id) => (dispatch) => {
	return dispatch(updateFriendData({_id: id}))
}
