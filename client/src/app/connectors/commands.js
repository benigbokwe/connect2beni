import axios from "axios"

export const makeGetRequest = (url, data = {}) => {
	//return a promise 
	return axios.get(url, data)
		.then((response) => response.data)
		.catch((error) => error)
}

export const makePostRequest = (url, data = {}) => {
	//return a promise
	return axios.post(url, data)
		.then((response) => response.data)
		.catch(error => error)
}

export const makeDeleteRequest = (url, data = {}) => {
	//return a promise
	return axios.delete(url, data)
		.then((response) => response)
		.catch(error => error)
}

export const makeUpdateRequest = (url, data = {}) => {
	//return a promise
	return axios.put(url, data)
		.then((response) => response.data)
		.catch(error => error)
}