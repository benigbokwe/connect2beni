import Immutable from "immutable"
import { createSelector } from "reselect"
import {createGetSelector} from "reselect-immutable-helpers"
import { getUi } from "../../selectors"

export const getFriends = createSelector(getUi, ({ friends }) => friends)
export const getFriendsList = createGetSelector(getFriends, "friendsList", Immutable.Map())
export const getFriendsId = createGetSelector(getFriends, "_id", String())
export const getShouldMakeRequest = createGetSelector(getFriends, "shouldMakeRequest", true)
// export const getFormInitialValues = createGetSelector(getFriends, "initialValues", Immutable.Map())

// Same as getFormInitialValues but might change in the future so lets get it seperated
export const getFriendData = createSelector(
    getFriendsList,
    getFriendsId,
    (friends, _id) => {
        return friends.get(_id)
    }
)

export const getFormInitialValues = createSelector(
    getFriendsList,
    getFriendsId,
    (friends, _id) => {
        return friends.get(_id)
    }
)