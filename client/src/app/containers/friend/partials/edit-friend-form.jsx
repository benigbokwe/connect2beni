import React from 'react'
import { Field, reduxForm } from 'redux-form'
import PropTypes from 'prop-types'

// components
import Button from '../../../components/button'

const EditFriendForm = (props) => {
    const {
        handleSubmit, 
        pristine,
        submitting,
        onSubmit
    } = props

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <Field className="form-control" name="firstName" component="input" type="text" />
            </div>
            <div className="form-group">
                <label htmlFor="lastName">Last Name</label>
                <Field className="form-control" name="lastName" component="input" type="text" />
            </div>
            <div className="form-group">
                <label htmlFor="email">Email</label>
                <Field className="form-control" name="email" component="input" type="email" />
            </div>
            <div className="form-group">
                <label htmlFor="phone">Phone</label>
                <Field className="form-control" name="phone" component="input" type="text" />
            </div>
            <div className="form-group">
                <label htmlFor="message">Profile message</label>
                <Field className="form-control" name="message" component="textarea" type="email" />
            </div>
            <Button type="submit" className="btn-primary" disabled={pristine || submitting}>Submit</Button>
        </form>
    )
}

EditFriendForm.propTypes = {
    handleSubmit: PropTypes.func, 
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    onSubmit: PropTypes.func
}


export default reduxForm({
    // a unique name for the form
    form: 'EditFriendsForm'
})(EditFriendForm)