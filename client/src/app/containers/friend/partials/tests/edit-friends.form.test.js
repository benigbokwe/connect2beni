import {shallow, configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'

configure({ adapter: new Adapter() })

import EditFriendForm from '../edit-friend-form'

/* eslint-env jest */

test('EditFriendForm renders without errors', () => {
    const wrapper = shallow(<EditFriendForm/>)
    expect(wrapper.length).toBe(1)
})

