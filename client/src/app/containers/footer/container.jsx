import React from 'react'

const Footer = () => (
  <footer className="text-muted mt-5">
    <div className="container">
      <p className="float-right">
        <button> Back to top </button>
      </p>
      <p>
          &copy; {'{Connect2Beni}'} {new Date().getFullYear()}, Funky way of getting job done!
      </p>
    </div>
  </footer>
)

export default Footer;