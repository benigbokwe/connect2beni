import React, { Component } from 'react'
import PropTypes from 'prop-types'

// import containers
import Header from '../header/container'
import Footer from '../footer/container'

import 'bootstrap/dist/css/bootstrap.min.css'

class App extends Component {
  render() {
    return (
        <div>
            <Header />
                <main role="main">{this.props.children}</main>
            <Footer />
        </div>
    )
  }
}

App.propTypes = {
    children: PropTypes.node.isRequired
}

export default App