import {
	makeGetRequest,
	makePostRequest,
	makeDeleteRequest,
	makeUpdateRequest
} from "./commands"

// utils
import { reOrderById } from '../utils/utils'

import {
	receiveFriendsData,
	saveFriendData,
	deleteFriendData,
	updateFriendData
} from "../actions/create-actions"

export const initFriendsData = (url) => (dispatch) => {
	return makeGetRequest(url)
		.then((response) => {
			dispatch(receiveFriendsData({
				friendsList: reOrderById(response.friendsList)
			}))
		})
}

export const initSaveFriendsData = (url, options) => (dispatch) => {
	return makePostRequest(url, options)
		.then((response) => {
			dispatch(saveFriendData({
				friendsList: reOrderById(response.friend)
			}))
		})
}

export const initUpdateFriendsData = (url, options) => (dispatch) => {
	return makeUpdateRequest(url, options)
		.then((response) => {
			dispatch(updateFriendData(reOrderById(response.friend)))
		})
}

export const initDeleteFriendsData = (url, id) => (dispatch) => {
	return makeDeleteRequest(url)
		.then(() => {
			dispatch(deleteFriendData({_id: id}))
		})
}