import {LIVE_BASE_URLS} from '../constants'

export const getEndPointURL = () => {
    const env = process.env.NODE_ENV
    if (env === 'development') {
        return LIVE_BASE_URLS.dev
    }
    return env === 'staging' ? LIVE_BASE_URLS.staging : LIVE_BASE_URLS.prod
}

export const reOrderById = (data) => {
    const items = {}
    if (Array.isArray(data) && data.length > 0) {
        for(let i = 0; i <= data.length; i++) {
            if (data[i] instanceof  Object) {
                items[data[i]._id] = data[i]
           }
        }
    } else if(data && Object.keys(data).length > 0) {
        items[data._id] = data
    }

    return items
}