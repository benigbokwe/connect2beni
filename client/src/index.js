
import React from 'react'
import { render } from 'react-dom'
import Routes from './app/router'
import configureStore from './store/configureStore'

import 'jquery'
import 'popper.js'
import 'bootstrap'

import registerServiceWorker from './registerServiceWorker'

const preloadedState = window.__PRELOADED_STATE__
let store = configureStore(preloadedState)

render(<Routes store={store} />, document.getElementById('root'))

registerServiceWorker()
