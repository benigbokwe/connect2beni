import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

/**
 * Card component is used to show content in a card
 */

const Card = ({
    className,
    image,
    children,
    footer,
    hasShadow,
    hasBorder
}) => {
    const classes = classNames('c-card', {
        'c--shadow': hasShadow,
        'c--border': hasBorder,
    }, className)

    return (
        <article className={classes}>
            <div className="c-card__inner">
                {image &&
                    <div className="c-card-img">
                        {image}
                    </div>
                }
                <div className="c-card__body">
                    {children}
                </div>
            </div>
        </article>
    )
}


Card.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    hasBorder: PropTypes.bool,
    hasShadow: PropTypes.bool,
    image: PropTypes.node,
}

export default Card
