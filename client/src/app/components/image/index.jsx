import React from 'react'
import PropTypes from 'prop-types'

const Image = ({
    alt,
    className, 
    src,
    width,
    height,
    children
}) => (
    <img
        src={src}
        alt={alt}
        className={className}
        width={width}
        height={height}
    />
)

Image.propTypes = {
    alt: PropTypes.string,
    className: PropTypes.string,
    src: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string
}

export default Image