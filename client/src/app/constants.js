const DEVELOPMENT_BASE_URL = 'http://localhost:3002'
const STAGING_BASE_URL = 'http://staging-api.connect2beni.com'
const PRODUCTION_BASE_URL = 'http://api.connect2beni.com'

export const LIVE_BASE_URLS = {
    dev: DEVELOPMENT_BASE_URL,
    staging: STAGING_BASE_URL,
    prod: PRODUCTION_BASE_URL
}