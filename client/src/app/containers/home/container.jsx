import React from 'react'

const Home = () => (
    <div>
        <section>
            <div className="container">
                Welcome to my funky home page!
            </div>
        </section>
    </div>
)

export default Home